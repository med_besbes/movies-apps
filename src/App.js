import React from 'react';
import LogoAnimation from './components/LogoAnimation';
import Home from './components/Home';



function App() {
  

  return (
    <div>
      <Home />
      <LogoAnimation />
    </div>

  );
}

export default App;
