import React from 'react';
import { useSelector } from 'react-redux';
import MovieCard from '../components/MovieCard';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const MoviesGrid = ({ hasLoaded }) => {
  const classes = useStyles();
  const movies = useSelector(state => state.movies);
  
  return (
    <div className={classes.root} isLoaded={hasLoaded}>
      <Grid container spacing={3}>
      {movies.all.length < 1 && hasLoaded ? (
        <Grid item xs={12}>
          <Paper className={classes.paper}>Pas de Film</Paper>
        </Grid>
      ) : (

            movies.all.length > 0 &&
            movies.displayed.map((movie, i) => {
              return <MovieCard movie={movie} key={movie.id} />;
            })
      )}
       </Grid>
    </div>
  );
};

export default MoviesGrid;
