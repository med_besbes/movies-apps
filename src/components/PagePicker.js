import React from 'react'
import { useDispatch } from 'react-redux';
import { changeElementsPerPage } from '../actions';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    formControl: {
      //margin: theme.spacing(1),
      minWidth: '100%',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

const PagePicker = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    return (
        <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="demo-simple-select-outlined-label">Movies Par Page</InputLabel>
            <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                onChange={e =>
                    dispatch(changeElementsPerPage(Number(e.target.value)))
                }
                label="Movies Par Page"
            >
                <MenuItem value={4}>4 movies par page</MenuItem>
                <MenuItem value={8}>8 movies par page</MenuItem>
                <MenuItem value={12}>12 movies par page</MenuItem>
            </Select>
        </FormControl>
    )
}
export default PagePicker;
