import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSelectedCategory,
  removeSelectedCategory,
  resetSelectedCategories,
} from '../actions';
import { makeStyles } from '@material-ui/core/styles';

import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1.5),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));
const CategoriesPicker = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories);

  const handleCategoryClick = (isSelected, category) => {
    if (isSelected) {
      dispatch(removeSelectedCategory(category));
    } else {
      dispatch(addSelectedCategory(category));
    }
  };

  return (

    <div className={classes.root}>
      
      {categories.selected.length > 0 && (
        <Chip label="Tous" variant="outlined" onClick={() => dispatch(resetSelectedCategories())}  />
      )}
      
      {categories.all.map(category => {
        const isSelected = categories.selected.includes(category);
        return (
            <Chip label={category} variant="outlined" onClick={() => handleCategoryClick(isSelected, category)}    />
        );
      })}
    </div>

  );
};

export default CategoriesPicker;
