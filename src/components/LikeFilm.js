import React from 'react'
import { useDispatch } from 'react-redux';
import { AiFillLike, AiFillDislike } from "react-icons/ai";
import { likeMovie } from '../actions';
import Button from '@material-ui/core/Button';


const LikeFilm = ({ movie, likes, dislikes, etat, etat2 }) => {
  const dispatch = useDispatch();
  const handleLike = isLiked => {
    dispatch(likeMovie(movie.id));
  };
  return (
    <div>
      <Button onClick={() => handleLike(movie.isLiked)} color="primary" disabled={etat}><AiFillLike /></Button>
      {likes > 1000 ? (
        <span >{Math.round(parseInt(`${likes}` / 1000))} K</span>
      )
        : (
          <span >{likes} </span>
        )}

      <Button onClick={() => handleLike(movie.isLiked)} color="secondary" disabled={etat2}><AiFillDislike /></Button>
      {dislikes > 1000 ? (
        <span >{Math.round(parseInt(`${dislikes}` / 1000))} K</span>
      )
        : (
          <span >{dislikes} </span>
        )}
    </div>
  )
}

export default LikeFilm;
