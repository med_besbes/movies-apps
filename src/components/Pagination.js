import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { changePage } from '../actions';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const Pagination = ({ hasLoaded }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const pagination = useSelector(state => state.pagination);

  return (
    <div className={classes.root} isLoaded={hasLoaded}>
      <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
        <Button  disabled={pagination.current <= 1} onClick={() => {
        dispatch(changePage(pagination.current - 1));
      }}>Précédent</Button>
        <Button>{pagination.current}</Button>
        <Button
          onClick={() => {
          dispatch(changePage(pagination.current + 1));
        }} >Suivant</Button>
      </ButtonGroup>
    </div>
  );
};

export default Pagination;
