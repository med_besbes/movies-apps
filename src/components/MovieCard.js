import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { AiOutlineDelete } from "react-icons/ai";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { deleteMovie } from '../actions';
import Swal from 'sweetalert2';

import LikeFilm from './LikeFilm'
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    bullet: {
      //display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }),
);

const MovieCard = ({ movie }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const handleDeleteButton = () => {
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez pas récupérer en arrière !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, supprimez-le !'
    }).then((result) => {
      if (result.isConfirmed) {
        handleDelete(movie);
        Swal.fire(
          'Supprimé !',
          'Votre fichier a été supprimé.',
          'Succès'
        )
      }
    })
  };

  const handleDelete = movie => {

    dispatch(deleteMovie(movie.id));
  };

  return (
    <Grid item xs={6} sm={3}>
      <Paper className={classes.paper}>
        <Card variant="outlined" >
          <CardContent>
            <Typography className={classes.title} color="textSecondary" gutterBottom>
              <AiOutlineDelete onClick={() => handleDeleteButton(movie)} />
            </Typography>
            <Typography variant="h5" component="h2">
              {movie.title}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {movie.category}
            </Typography>
          </CardContent>
          <CardActions>
            {
              movie.isLiked === undefined ? (
                <LikeFilm movie={movie} likes={movie.likes} dislikes={movie.dislikes} />
              ) : movie.isLiked ? (
                <LikeFilm movie={movie} likes={movie.likes} dislikes={movie.dislikes} etat={true} />
              ) : (
                <LikeFilm movie={movie} likes={movie.likes} dislikes={movie.dislikes} etat2={true} />
              )
            }
          </CardActions>
        </Card>
      </Paper>
    </Grid>
  );
};

export default MovieCard;
