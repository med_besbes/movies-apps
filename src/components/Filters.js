import React from 'react';
import CategoriesPicker from '../components/CategoriesPicker';
import PagePicker from '../components/PagePicker';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 500,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Filters = ({ hasLoaded }) => {
  const classes = useStyles();
  return (
    <div className={classes.root} isLoaded={hasLoaded}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <CategoriesPicker />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <PagePicker />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Filters;
