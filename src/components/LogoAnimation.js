import React from 'react'

const LogoAnimation = () => {
    return (
        <div>
            <div class="logo-wrapper">
                <div class="circle"></div>
                <div class="line line3"></div>
                <div class="line2 line4"></div>
                <div class="linelong line5"></div>
            </div>
        </div>
    )
}

export default LogoAnimation;