import React, { useState, useEffect,useLayoutEffect  } from 'react';
import MoviesGrid from './MoviesGrid';
import Pagination from './Pagination';
import Filters from './Filters';
import { movies$ } from '../services/movies';
import { useDispatch, useSelector } from 'react-redux';
import {
  loadMovies,
  setDisplayedMovies,
  setMaxPage,
  setAllCategories,
} from '../actions';



function Home() {
  const dispatch = useDispatch();
  const movies = useSelector(state => state.movies);
  const elementsPerPage = useSelector(state => state.elementsPerPage);
  const pagination = useSelector(state => state.pagination);
  const categories = useSelector(state => state.categories);
  const [hasLoaded, setHasloaded] = useState(false);

  useEffect (() => {
    movies$.then(result => {
      setHasloaded(true);
      dispatch(loadMovies(result));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);



  useLayoutEffect (() => {
    let moviesCopy = [...movies.all];
  
    const tempCategories = [];
    for (var i = 0; i < moviesCopy.length; i++) {
      let movieCat = moviesCopy[i].category;

      if (!tempCategories.includes(movieCat)) {
        tempCategories.push(movieCat);
      }
    }
    dispatch(setAllCategories(tempCategories));
    if (categories.selected.length > 0) {
      moviesCopy = moviesCopy.filter(movie =>
        categories.selected.includes(movie.category)
      );
    }

    dispatch(setMaxPage(moviesCopy.length / elementsPerPage));
    dispatch(
      setDisplayedMovies(
        moviesCopy.slice(
          elementsPerPage * (pagination.current - 1),
          elementsPerPage * (pagination.current - 1) + elementsPerPage
        )
      )
    );
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [movies.all, elementsPerPage, pagination.current, categories.selected]);


  return (
    
    <div>
      {console.log(useSelector(state => state.movies))}
      {console.log(useSelector(state => state.elementsPerPage))}
      {console.log(useSelector(state => state.pagination))}
      {console.log(useSelector(state => state.categories))}
      <Filters hasLoaded={hasLoaded} />
      <MoviesGrid hasLoaded={hasLoaded} />
      <Pagination hasLoaded={hasLoaded} />

    </div>

  );
}

export default Home;
